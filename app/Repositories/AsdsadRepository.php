<?php

namespace App\Repositories;

use App\Models\Asdsad;
use InfyOm\Generator\Common\BaseRepository;

class AsdsadRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sdf'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Asdsad::class;
    }
}
