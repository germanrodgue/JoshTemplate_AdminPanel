<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateAsdsadRequest;
use App\Http\Requests\UpdateAsdsadRequest;
use App\Repositories\AsdsadRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Asdsad;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class AsdsadController extends InfyOmBaseController
{
    /** @var  AsdsadRepository */
    private $asdsadRepository;

    public function __construct(AsdsadRepository $asdsadRepo)
    {
        $this->asdsadRepository = $asdsadRepo;
    }

    /**
     * Display a listing of the Asdsad.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->asdsadRepository->pushCriteria(new RequestCriteria($request));
        $asdsads = $this->asdsadRepository->all();
        return view('admin.asdsads.index')
            ->with('asdsads', $asdsads);
    }

    /**
     * Show the form for creating a new Asdsad.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.asdsads.create');
    }

    /**
     * Store a newly created Asdsad in storage.
     *
     * @param CreateAsdsadRequest $request
     *
     * @return Response
     */
    public function store(CreateAsdsadRequest $request)
    {
        $input = $request->all();

        $asdsad = $this->asdsadRepository->create($input);

        Flash::success('Asdsad saved successfully.');

        return redirect(route('admin.asdsads.index'));
    }

    /**
     * Display the specified Asdsad.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $asdsad = $this->asdsadRepository->findWithoutFail($id);

        if (empty($asdsad)) {
            Flash::error('Asdsad not found');

            return redirect(route('asdsads.index'));
        }

        return view('admin.asdsads.show')->with('asdsad', $asdsad);
    }

    /**
     * Show the form for editing the specified Asdsad.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $asdsad = $this->asdsadRepository->findWithoutFail($id);

        if (empty($asdsad)) {
            Flash::error('Asdsad not found');

            return redirect(route('asdsads.index'));
        }

        return view('admin.asdsads.edit')->with('asdsad', $asdsad);
    }

    /**
     * Update the specified Asdsad in storage.
     *
     * @param  int              $id
     * @param UpdateAsdsadRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAsdsadRequest $request)
    {
        $asdsad = $this->asdsadRepository->findWithoutFail($id);

        

        if (empty($asdsad)) {
            Flash::error('Asdsad not found');

            return redirect(route('asdsads.index'));
        }

        $asdsad = $this->asdsadRepository->update($request->all(), $id);

        Flash::success('Asdsad updated successfully.');

        return redirect(route('admin.asdsads.index'));
    }

    /**
     * Remove the specified Asdsad from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.asdsads.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = Asdsad::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.asdsads.index'))->with('success', Lang::get('message.success.delete'));

       }

}
