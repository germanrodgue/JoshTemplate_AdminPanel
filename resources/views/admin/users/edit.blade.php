@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Edit User
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
  

@stop


{{-- Page content --}}
@section('content')
<section id="icon-tabs">
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Edit user</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                </div>
            </div>
            {!! Form::model($user, ['url' => URL::to('admin/users/'. $user->id.''), 'method' => 'put', 'class' => 'form-horizontal','id'=>'commentForm', 'enctype'=>'multipart/form-data','files'=> true]) !!}
                                    {{ csrf_field() }}
            <div class="card-content collapse show">
                <div class="card-body">
                    <form action="#" class="icons-tab-steps wizard-circle">

                        <!-- Step 1 -->
                        <h6><i class="step-icon ft-home"></i> Change information </h6>
                        <fieldset>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->first('first_name', 'has-error') }}">
                                        <label for="firstName2">First Name :</label>
                                        <input id="first_name" name="first_name" type="text"
                                                               placeholder="First Name" class="form-control required"
                                                               value="{!! old('first_name', $user->first_name) !!}"/>
                                    </div>
                                    {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->first('last_name', 'has-error') }}">
                                        <label for="lastName2">Last Name :</label>
                                        <input id="last_name" name="last_name" type="text" placeholder="Last Name"
                                                               class="form-control required"
                                                               value="{!! old('last_name', $user->last_name) !!}"/>
                                    </div>
                                    {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->first('email', 'has-error') }}">
                                        <label for="emailAddress3">Email Address :</label>
                                        <input id="email" name="email" placeholder="E-Mail" type="text"
                                        class="form-control required email"
                                        value="{!! old('email', $user->email) !!}"/>

                             {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
    
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->first('password', 'has-error') }}">
                                                <label for="password">Password :</label>
                                                <input id="password" name="password" type="password" placeholder="Password"
                                                class="form-control" value="{!! old('password') !!}"/>
                                            </div>
                                            {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                                        </div>
                                </div>
                                <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="photo">Photo :</label>
                                                <label class="custom-file">
                                                    <input type="file" name="pic_file" id="pic_file" class="custom-file-input">
                                                    <span class="custom-file-control text-center">Choose file</span>
                                                </label>
                                            </div>
                                        </div>       
                                        <div class="col-md-6">
                                        <div class="form-group required">
                                            <label for="group" >Group :</label>
                                            
                                                <select class="form-control required" title="Select group..." name="group"
                                                        id="group">
                                                    <option value="">Select</option>
                                                    @foreach($groups as $group)
                                                        <option value="{{ $group->id }}"
                                                                @if($group->id == old('group')) selected="selected" @endif >{{ $group->name}}</option>
                                                    @endforeach
                                                </select>
                                                {!! $errors->first('group', '<span class="help-block">:message</span>') !!}
                                            </div>
                                            <span class="help-block">{{ $errors->first('group', ':message') }}</span>
                                        </div>  
                                </div>
                                            <div class="form-group">
                                                    <label for="activate" class="col-sm-2 control-label mt-2"> Activate User</label>
                                                    <div class="col-sm-10">
                                                        <input id="activate" name="activate" type="checkbox" class="pos-rel p-l-30 custom-checkbox" value="1" @if($status) checked="checked" @endif  >
                                                        <span>To activate your account click the check box</span>
                                                    </div>
                                                </div>
                                                <div class="form-actions " style="justify-content: center; display: flex;">
	                            <button type="reset" class="btn btn-warning mr-1">
	                            	<i class="ft-x"></i> Reset
	                            </button>
	                            <button type="submit" class="btn btn-primary">
	                                <i class="fa fa-check-square-o"></i> Save
	                            </button>
	                        </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
   
@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script>
        function formatState (state) {
            if (!state.id) { return state.text; }
            var $state = $(
                '<span><img src="{{asset('assets/img/countries_flags')}}/'+ state.element.value.toLowerCase() + '.png" class="img-flag" width="20px" height="20px" /> ' + state.text + '</span>'
            );
            return $state;

}
$(".country_field").select2({
    templateResult: formatState,
    templateSelection: formatState,
    placeholder: "select a country",
    theme:"bootstrap"
});


</script>
@stop
