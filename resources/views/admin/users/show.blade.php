@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    View User Details
    @parent
@stop

{{-- page level styles --}}

@section('header_styles')
    <meta name="csrf_token" content="{{ csrf_token() }}">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/vendors/x-editable/css/bootstrap-editable.css') }}" rel="stylesheet"/>

    <link href="{{ asset('assets/css/pages/user_profile.css') }}" rel="stylesheet"/>
@stop


{{-- Page content --}}
@section('content')
<div class="container">
<div class="row my-2">
    <div class="col-lg-8 order-lg-2">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">Profile</a>
            </li>
            
            <li class="nav-item">
                    <a href="" data-target="#edit" data-toggle="tab" class="nav-link">Edit</a>
            </li>
        </ul>
        <div class="tab-content py-4">
            <div class="tab-pane active" id="profile">
                 <table class="table table-bordered table-striped" id="users">

                                                        <tr>
                                                            <td>@lang('users/title.first_name')</td>
                                                            <td>
                                                                <p class="user_name_max">{{ $user->first_name }}</p>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td>@lang('users/title.last_name')</td>
                                                            <td>
                                                                <p class="user_name_max">{{ $user->last_name }}</p>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td>@lang('users/title.email')</td>
                                                            <td>
                                                                {{ $user->email }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>@lang('users/title.status')</td>
                                                            <td>

                                                                @if($user->deleted_at)
                                                                    Deleted
                                                                @elseif($activation = Activation::completed($user))
                                                                    Activated
                                                                @else
                                                                    Pending
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>@lang('users/title.created_at')</td>
                                                            <td>
                                                                {!! $user->created_at->diffForHumans() !!}
                                                            </td>
                                                        </tr>
                                                    </table>
                <!--/row-->
            </div>
            
          
                <div class="tab-pane row" id="edit">
                {!! Form::model($user, ['url' => URL::to('admin/users/'. $user->id.''), 'method' => 'put', 'class' => 'form-horizontal','id'=>'commentForm', 'enctype'=>'multipart/form-data','files'=> true]) !!}
                                    {{ csrf_field() }}
           
              <div class="col-md-12">
                    <form action="#" class="icons-tab-steps wizard-circle">

                        <!-- Step 1 -->
                        <h6><i class="step-icon ft-home"></i> Change information </h6>
                        <fieldset>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->first('first_name', 'has-error') }}">
                                        <label for="firstName2">First Name :</label>
                                        <input id="first_name" name="first_name" type="text"
                                                               placeholder="First Name" class="form-control required"
                                                               value="{!! old('first_name', $user->first_name) !!}"/>
                                    </div>
                                    {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->first('last_name', 'has-error') }}">
                                        <label for="lastName2">Last Name :</label>
                                        <input id="last_name" name="last_name" type="text" placeholder="Last Name"
                                                               class="form-control required"
                                                               value="{!! old('last_name', $user->last_name) !!}"/>
                                    </div>
                                    {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->first('email', 'has-error') }}">
                                        <label for="emailAddress3">Email Address :</label>
                                        <input id="email" name="email" placeholder="E-Mail" type="text"
                                        class="form-control required email"
                                        value="{!! old('email', $user->email) !!}"/>

                             {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
    
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->first('password', 'has-error') }}">
                                        <label for="password">Password :</label>
                                        <input id="password" name="password" type="password" placeholder="Password"
                                        class="form-control" value="{!! old('password') !!}"/>
                                    </div>
                                    {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                                            
                                                                        <h6>Upload a different photo</h6>
                                <label class="custom-file">
                                    <input type="file" name="pic_file" id="pic_file" class="custom-file-input">
                                    <span class="custom-file-control">Choose file</span>
                                </label>
                            
                                <div class="form-group">
                                                    <label for="activate" class="col-sm-2 control-label mt-2"> Activate User</label>
                                                    <div class="col-sm-10">
                                                        <input id="activate" name="activate" type="checkbox" class="pos-rel p-l-30 custom-checkbox" value="1" @if($status) checked="checked" @endif  >
                                                        <span>To activate this account click the check box</span>
                                                    </div>
                                                </div>    
                                                <div class="form-actions " style="justify-content: center; display: flex;">
	                            <button type="reset" class="btn btn-warning mr-1">
	                            	<i class="ft-x"></i> Reset
	                            </button>
	                            <button type="submit" class="btn btn-primary">
	                                <i class="fa fa-check-square-o"></i> Save
	                            </button>
	                        </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 order-lg-1 text-center">
        @if($user->pic)
            <img src="{{ asset('uploads/users/'. $user -> pic) }}"  alt="img" class="img-responsive"/>
        @else
            <img src="{{ asset('assets/images/authors/no_avatar.jpg') }}" alt="..." class="img-responsive"/>
        @endif
    </div>
    
</div>
</div>

@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <!-- Bootstrap WYSIHTML5 -->
    <script  src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#change-password').click(function (e) {
                e.preventDefault();
                var check = false;
                if ($('#password').val() ===""){
                    alert('Please Enter password');
                }
                else if  ($('#password').val() !== $('#password-confirm').val()) {
                    alert("confirm password should match with password");
                }
                else if  ($('#password').val() === $('#password-confirm').val()) {
                    check = true;
                }

                if(check == true){
                var sendData =  '_token=' + $("input[name='_token']").val() +'&password=' + $('#password').val() +'&id=' + {{ $user->id }};
                    var path = "passwordreset";
                    $.ajax({
                        url: path,
                        type: "post",
                        data: sendData,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
                        },
                        success: function (data) {
                            alert('password reset successful');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert('error in password reset');
                        }
                    });
                }
            });
        });
    </script>

@stop
