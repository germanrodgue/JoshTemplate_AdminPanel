@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Add User
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    
@stop


{{-- Page content --}}
@section('content')

	    <div class="col-md-12">
	        <div class="card">
	         
	            <div class="card-content collpase show">
	                <div class="card-body">
						
	                    <form class="form form-horizontal" id="commentForm" action="{{ route('admin.users.store') }}"
                              method="POST" enctype="multipart/form-data">
                            <!-- CSRF Token -->
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
	                    	<div class="form-body">
	                    		<h4 class="form-section"><i class="ft-user"></i> Personal Info</h4>
			                    <div class="form-group row {{ $errors->first('first_name', 'has-error') }}">
	                            	<label class="col-md-3 label-control" for="first_name">First Name</label>
		                            <div class="col-md-6">
                                        <input type="text" id="first_name" class="form-control" placeholder="First Name" name="first_name" value="{!! old('first_name') !!}">
                                        {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
		                            </div>
		                        </div>
		                        <div class="form-group row  {{ $errors->first('last_name', 'has-error') }}">
	                            	<label class="col-md-3 label-control" for="last_name">Last Name</label>
									<div class="col-md-6">
                                        <input type="text" id="last_name" class="form-control" placeholder="Last Name" name="last_name" value="{!! old('last_name') !!}" >
                                        {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
	                            	</div>
		                        </div>

		                        <div class="form-group row  {{ $errors->first('email', 'has-error') }}">
		                            <label class="col-md-3 label-control" for="email">E-mail</label>
		                            <div class="col-md-6">
                                        <input type="text" id="email" class="form-control" placeholder="E-mail" name="email">
                                        {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
		                            </div>
		                        </div>

		                        <div class="form-group row">
		                            <label class="col-md-3 label-control" for="password">Password</label>
		                            <div class="col-md-6">
                                        <input type="password" id="password" class="form-control" placeholder="Password" name="password">
                                        {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
		                            </div>
		                        </div>


                                <div class="form-group row {{ $errors->first('password_confirm', 'has-error') }}">
                                            <label for="password_confirm" class="col-md-3 label-control">Confirm Password</label>
                                            <div class="col-md-6">
                                                <input id="password_confirm" name="password_confirm" type="password"
                                                       placeholder="Confirm Password " class="form-control required"/>
                                                {!! $errors->first('password_confirm', '<span class="help-block">:message</span>') !!}
                                            </div>
										</div>
										<div class="form-group row required">
                                            <label for="group" class="col-md-3 label-control">Group *</label>
                                            <div class="col-md-6">
                                                <select class="form-control required" title="Select group..." name="group"
                                                        id="group">
                                                    <option value="">Select</option>
                                                    @foreach($groups as $group)
                                                        <option value="{{ $group->id }}"
                                                                @if($group->id == old('group')) selected="selected" @endif >{{ $group->name}}</option>
                                                    @endforeach
                                                </select>
                                                {!! $errors->first('group', '<span class="help-block">:message</span>') !!}
                                            </div>
                                            <span class="help-block">{{ $errors->first('group', ':message') }}</span>
                                        </div>
											<div class="form-group row text-center {{ $errors->first('pic_file', 'has-error') }}">
													<label for="pic" class="col-md-3 label-control">Avatar upload</label>

													<div class="col-md-6">
														<label class="custom-file">
															<input id="pic_file" type="file" name="pic_file" class="custom-file-input">
															<span class="custom-file-control">Choose file</span>
														</label>
													</div>
									
                                            </div>
                                                </div>
                                                <span class="help-block">{{ $errors->first('pic_file', ':message') }}</span>
                                            </div>
                                        </div>
								<!-- <h4 class="form-section"><i class="icon-clipboard4"></i> Requirements</h4>

		                        <div class="form-group row">
									<label class="col-md-3 label-control" for="projectinput5">Company</label>
									<div class="col-md-9">
		                            	<input type="text" id="projectinput5" class="form-control" placeholder="Company Name" name="company">
		                            </div>
		                        </div>

		                        <div class="form-group row">
		                        	<label class="col-md-3 label-control" for="projectinput6">Interested in</label>
		                        	<div class="col-md-9">
			                            <select id="projectinput6" name="interested" class="form-control">
			                                <option value="none" selected="" disabled="">Interested in</option>
			                                <option value="design">design</option>
			                                <option value="development">development</option>
			                                <option value="illustration">illustration</option>
			                                <option value="branding">branding</option>
			                                <option value="video">video</option>
			                            </select>
		                            </div>
		                        </div>

		                        <div class="form-group row">
		                        	<label class="col-md-3 label-control" for="projectinput7">Budget</label>
		                        	<div class="col-md-9">
			                            <select id="projectinput7" name="budget" class="form-control">
			                                <option value="0" selected="" disabled="">Budget</option>
			                                <option value="less than 5000$">less than 5000$</option>
			                                <option value="5000$ - 10000$">5000$ - 10000$</option>
			                                <option value="10000$ - 20000$">10000$ - 20000$</option>
			                                <option value="more than 20000$">more than 20000$</option>
			                            </select>
		                            </div>
		                        </div>

								<div class="form-group row">
									<label class="col-md-3 label-control">Select File</label>
									<div class="col-md-9">
										<label id="projectinput8" class="file center-block">
											<input type="file" id="file">
											<span class="file-custom"></span>
										</label>
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 label-control" for="projectinput9">About Project</label>
									<div class="col-md-9">
										<textarea id="projectinput9" rows="5" class="form-control" name="comment" placeholder="About Project"></textarea>
									</div>
								</div> -->
							</div>

	                        <div class="form-actions " style="justify-content: center; display: flex;">
	                            <button type="reset" id="type-success" class="btn btn-warning mr-1">
	                            	<i class="ft-x"></i> Reset
	                            </button>
	                            <button type="submit" class="btn btn-primary" >
	                                <i class="fa fa-check-square-o"></i> Save
	                            </button>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	
        
@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script>
        function formatState (state) {
            if (!state.id) { return state.text; }
            var $state = $(
                '<span><img src="{{ asset('assets/img/countries_flags') }}/'+ state.element.value.toLowerCase() + '.png" class="img-flag" width="20px" height="20px" /> ' + state.text + '</span>'
            );
            return $state;

        }
        $("#countries").select2({
            templateResult: formatState,
            templateSelection: formatState,
            placeholder: "select a country",
            theme:"bootstrap"
        });

    </script>
@stop
