@extends('admin/layouts/default')


{{-- page level styles --}}
@section('header_styles')
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/> -->

<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/> -->
<!-- <link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" /> -->

@stop


{{-- Page content --}}
@section('content')


<!-- Main content -->
<div class="app-content content">
<div class="content-wrapper">
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2">
      <h3 class="content-header-title mb-0">List Users</h3>
      <div class="row breadcrumbs-top">
        <div class="breadcrumb-wrapper col-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a>
            </li>
            <li class="breadcrumb-item"><a href="#">List Users</a>
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div class="content-header-right text-md-right col-md-6 col-12">
      <div class="btn-group">
        <button type="button" class="btn btn-round btn-primary"><i class="ft-settings pr-1"></i> Settings</button>
        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-round btn-primary dropdown-toggle"><span class="sr-only">Toggle Dropdown</span></button>
        <div class="dropdown-menu"><a href="#" class="dropdown-item">Action</a><a href="#" class="dropdown-item">Another action</a><a href="#" class="dropdown-item">Something else here</a>
          <div class="dropdown-divider"></div><a href="#" class="dropdown-item">Separated link</a>
        </div>
      </div>
    </div>
  </div>
  <div class="content-body"><!-- Basic Tables start -->
<div class="row">
<div class="col-12">
  <div class="card">
     
      <div class="card-content collapse show">
       
         
          <div class="table-responsive">
              <table id="table" class="table mb-0">
                  <thead>
                      <tr class="filters">
                          <th>ID</th>
                          <th>First Name</th>
                          <th>Last Name</th>
                          <th>Email</th>
                          <th>Status</th>
                          <th>Created</th>
                          <th>Actions</th>
                      </tr>
                  </thead>
                  <tbody>
                
                  </tbody>
              </table>
          </div>
          </div>
      </div>
  </div>
</div>
</div>


@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('dist/js/admin/index.js') }}"></script>


<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content"></div>
  </div>
</div>


@stop
