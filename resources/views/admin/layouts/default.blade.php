<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Spark</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{--CSRF Token--}}
  
    <!-- global css -->

    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/css/vendors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/vendors/css/charts/jquery-jvectormap-2.0.3.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/vendors/css/charts/morris.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/vendors/css/extensions/unslider.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/vendors/css/weather-icons/climacons.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/css/core/menu/menu-types/vertical-overlay-menu.css') }}">
    <link href="{{ asset('dist/vendors/css/extensions/toastr.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/css/pages/toastr.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/switchery/css/switchery.css') }}"/>

    @if(Request::url() === 'admin/')
    @yield('header_styles')
    @else
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/css/app.css') }}">
    @endif
    
<body class="vertical-layout vertical-overlay-menu 2-columns fixed-navbar pace-done menu-collapsed" data-open="click" data-menu="vertical-overlay-menu" data-col="2-columns">

            @include('admin.theme.header')
            
            @if(Sentinel::check()) 
                @include('admin.theme.sidebar')
            @endif

            @yield('content')

<!-- global js -->
    <script src="{{ asset('assets/js/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('dist/vendors/js/extensions/jquery.knob.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('dist/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('dist/vendors/js/charts/raphael-min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('dist/vendors/js/charts/morris.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('dist/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('dist/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js') }}" type="text/javascript"></script>
    <script src="{{ asset('dist/data/jvector/visitor-data.js') }}" type="text/javascript"></script>
    <script src="{{ asset('dist/vendors/js/charts/chart.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('dist/vendors/js/charts/jquery.sparkline.min.js') }} " type="text/javascript"></script>
    <script src="{{ asset('dist/vendors/js/extensions/unslider-min.js') }} " type="text/javascript"></script>
    <script src="{{ asset('dist/js/core/app-menu.js') }}" type="text/javascript"></script>
    <script src="{{ asset('dist/js/core/app.js') }}" type="text/javascript"></script>
    <script src="{{ asset('dist/js/scripts/pages/dashboard-analytics.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/livicons-1.4.min.js') }}"></script>
    <!-- <script src="{{ asset('assets/js/app.js') }}" type="text/javascript"></script> -->
    <script src="{{ asset('dist/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    <script src="{{ asset('assets/js/app.js') }}" type="text/javascript"></script>

    {!! Toastr::message() !!}
    @yield('footer_scripts')
</body>
</html>
