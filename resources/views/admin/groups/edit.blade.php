@extends('admin/layouts/default')

{{-- Web site Title --}}
@section('title')
@lang('groups/title.edit')
@parent
@stop

{{-- Content --}}
@section('content')
<section class="content-header">
    <h1>
        @lang('groups/title.edit')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                @lang('general.dashboard')
            </a>
        </li>
        <li>@lang('groups/title.groups')</li>
        <li class="active">@lang('groups/title.edit')</li>
    </ol>
</section>

<!-- Main content -->
<section id="icon-tabs">
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Edit user</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                </div>
            </div>
            @if($role)
            {!! Form::model($role, ['url' => URL::to('admin/groups/'. $role->id), 'method' => 'put', 'class' => 'form-horizontal']) !!}
            <!-- CSRF Token -->
                {{ csrf_field() }}
            <div class="card-content collapse show">
                <div class="card-body">
                    <form action="#" class="icons-tab-steps wizard-circle">

                        <!-- Step 1 -->
                        <h6><i class="step-icon ft-home"></i> Change information </h6>
                        <fieldset>
                            <div class="row">
                                <div class="col-md-6">
                                <div class="form-group {{ $errors->first('first_name', 'has-error') }}">
                                        <label for="firstName2">Name :</label>
                                        <input type="text" id="name" name="name" class="form-control"
                                        placeholder=@lang('groups/form.name') value="{!! old('name', $role->name) !!}">
                                {!! $errors->first('name', '<span class="help-block">:message</span>') !!}  
                                </div>
                                <div class="form-group">
                                    <label for="slug" class="col-sm-2 control-label">@lang('groups/form.slug')</label>
                                    
                                        <input type="text" class="form-control" value="{!! $role->slug !!}" readonly />
                                
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-4">
                                        <a class="btn btn-danger" href="{{ route('admin.groups.index') }}">
                                            @lang('button.cancel')
                                        </a>
                                        <button type="submit" class="btn btn-success">
                                            @lang('button.save')
                                        </button>
                                    </div>
                                </div>

                             
	                           
	                        </div>
                        </fieldset>
                    </form>
                    @else
                        <h1>@lang('groups/message.error.no_role_exists')</h1>
                            <a class="btn btn-danger" href="{{ route('admin.groups.index') }}">
                                @lang('button.back')
                            </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
</section>


@stop
