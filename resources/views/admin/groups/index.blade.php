@extends('admin/layouts/default')

{{-- Web site Title --}}
@section('title')
@lang('groups/title.management')
@parent
@stop

{{-- Content --}}
@section('content')
<!-- Main content -->
<div class="app-content content">
<div class="content-wrapper">
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2">
      <h3 class="content-header-title mb-0">List Users</h3>
      <div class="row breadcrumbs-top">
        <div class="breadcrumb-wrapper col-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a>
            </li>
            <li class="breadcrumb-item"><a href="#">List Users</a>
            </li>
          </ol>
        </div>
        <div class="pull-right">
            <a href="{{ route('admin.groups.create') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
        </div>
      </div>
    </div>
    <div class="content-header-right text-md-right col-md-6 col-12">
      <div class="btn-group">
        <button type="button" class="btn btn-round btn-primary"><i class="ft-settings pr-1"></i> Settings</button>
        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-round btn-primary dropdown-toggle"><span class="sr-only">Toggle Dropdown</span></button>
        <div class="dropdown-menu"><a href="#" class="dropdown-item">Action</a><a href="#" class="dropdown-item">Another action</a><a href="#" class="dropdown-item">Something else here</a>
          <div class="dropdown-divider"></div><a href="#" class="dropdown-item">Separated link</a>
        </div>
      </div>
    </div>
  </div>
  <div class="content-body"><!-- Basic Tables start -->
<div class="row">
<div class="col-12">
  <div class="card">
     
      <div class="card-content collapse show">
       
         
          <div class="table-responsive">
              <table id="table" class="table mb-0">
                  <thead>
                      <tr class="filters">
                      <th>ID</th>
                      <th>Name</th>
                      <th>N. of users</th>
                      <th>Created At</th>
                      <th>Actions</th>
                      </tr>
                  </thead>
                  <tbody>
                  @foreach ($roles as $role)
                                <tr>
                                    <td>{!! $role->id !!}</td>
                                    <td>{!! $role->name !!}</td>
                                    <td>{!! $role->users()->count() !!}</td>
                                    <td>{!! $role->created_at->diffForHumans() !!}</td>
                                    <td>
                                        <a href="{{ route('admin.groups.edit', $role->id) }}">
                                                <i class="livicon" data-name="edit" data-size="25" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit group"></i>
                                            </a>
                                            <!-- let's not delete 'Admin' group by accident -->
                                            @if ($role->id !== 1)
                                                @if($role->users()->count())
                                                    <a href="#" data-toggle="modal" data-target="#users_exists" data-name="{!! $role->name !!}" class="users_exists">
                                                        <i class="livicon" data-name="warning-alt" data-size="25"
                                                           data-loop="true" data-c="#f56954" data-hc="#f56954"
                                                           title="@lang('groups/form.users_exists')"></i>
                                                    </a>
                                                @else
                                                    <a href="{{ route('admin.groups.confirm-delete', $role->id) }}" data-toggle="modal" data-target="#delete_confirm">
                                                        <i class="livicon" data-name="remove-alt" data-size="25"
                                                           data-loop="true" data-c="#f56954" data-hc="#f56954"
                                                           title="@lang('groups/form.delete_group')"></i>
                                                    </a>
                                                @endif

                                            @endif
                                    </td>
                                </tr>
                                @endforeach
                  </tbody>
              </table>
          </div>
          </div>
      </div>
  </div>
</div>
</div>



@stop

{{-- Body Bottom confirm modal --}}
@section('footer_scripts')
<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    </div>
  </div>
</div>
<div class="modal fade" id="users_exists" tabindex="-2" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                @lang('groups/message.users_exists')
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});
    $(document).on("click", ".users_exists", function () {

        var group_name = $(this).data('name');
        $(".modal-header h4").text( group_name+" Group" );
    });</script>
@stop
