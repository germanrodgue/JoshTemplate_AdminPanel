@extends('admin/layouts/default')

@section('content')
<main class="container pt-5">
<table class="table table-bordered table-definition mb-5">
<thead class="table-success">
    <tr>
        <th></th>
        <th>Name</th>
        <th>Registration Date</th>
        <th>E-mail address</th>
        <th>Products</th>
    </tr>
</thead>
<tbody>
    <tr>
        <td>
            <label class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input">
                <span class="custom-control-indicator"></span>
            </label>
        </td>
        <td>Proveedor</td>
        <td>September 14, 2013</td>
        <td>Proveedor@yahoo.com</td>
        <td>No</td>
    </tr>
    <tr>
        <td>
            <label class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input">
                <span class="custom-control-indicator"></span>
            </label>
        </td>
        <td>Proveedor</td>
        <td>September 14, 2013</td>
        <td>Proveedor@yahoo.com</td>
        <td>No</td>
    </tr>
    <tr>
        <td>
            <label class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input">
                <span class="custom-control-indicator"></span>
            </label>
        </td>
        <td>Proveedor</td>
        <td>September 14, 2013</td>
        <td>Proveedor@yahoo.com</td>
        <td>No</td>
    </tr>
</tbody>
<tfoot>
    <tr>
        <th></th>
        <th colspan="4">
            <button class="btn btn-primary float-right">Add User</button>
            <button class="btn btn-default">Approve</button>
            <button class="btn btn-default">Approve All</button>
        </th>
    </tr>
</tfoot>
</table>
</main>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('dist/js/admin/index.js') }}"></script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content"></div>
    </div>
</div>

@stop