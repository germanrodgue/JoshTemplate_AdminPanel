<?php

/* custom routes generated by CRUD */


Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {

Route::get('asdsads', ['as'=> 'asdsads.index', 'uses' => 'AsdsadController@index']);
Route::post('asdsads', ['as'=> 'asdsads.store', 'uses' => 'AsdsadController@store']);
Route::get('asdsads/create', ['as'=> 'asdsads.create', 'uses' => 'AsdsadController@create']);
Route::put('asdsads/{asdsads}', ['as'=> 'asdsads.update', 'uses' => 'AsdsadController@update']);
Route::patch('asdsads/{asdsads}', ['as'=> 'asdsads.update', 'uses' => 'AsdsadController@update']);
Route::get('asdsads/{id}/delete', array('as' => 'asdsads.delete', 'uses' => 'AsdsadController@getDelete'));
Route::get('asdsads/{id}/confirm-delete', array('as' => 'asdsads.confirm-delete', 'uses' => 'AsdsadController@getModalDelete'));
Route::get('asdsads/{asdsads}', ['as'=> 'asdsads.show', 'uses' => 'AsdsadController@show']);
Route::get('asdsads/{asdsads}/edit', ['as'=> 'asdsads.edit', 'uses' => 'AsdsadController@edit']);

});
